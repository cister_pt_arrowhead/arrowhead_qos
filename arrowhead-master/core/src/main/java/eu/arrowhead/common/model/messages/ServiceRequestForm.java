package eu.arrowhead.common.model.messages;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlRootElement;

import eu.arrowhead.common.model.ArrowheadService;
import eu.arrowhead.common.model.ArrowheadSystem;

@XmlRootElement
public class ServiceRequestForm {

	private ArrowheadService requestedService;
	private ArrowheadSystem requesterSystem;
	private Map<String, Boolean> orchestrationFlags = new HashMap<>();
	private HashMap<String,String> requestedQoS;
	private HashMap<String,String> commands;
	
	public ServiceRequestForm (){
		super();
	}

	public ServiceRequestForm(ArrowheadService requestedService, HashMap<String,String> requestedQoS, ArrowheadSystem requesterSystem) {
		this.requestedService = requestedService;
		this.requestedQoS = requestedQoS;
		this.requesterSystem = requesterSystem;
		this.orchestrationFlags.put("matchmaking", false);
		this.orchestrationFlags.put("externalServiceRequest", false);
		this.orchestrationFlags.put("triggerInterCloud", false);
		this.orchestrationFlags.put("metadataSearch", false);
		this.orchestrationFlags.put("pingProvider", false);
	}

	public ServiceRequestForm(ArrowheadService requestedService, ArrowheadSystem requesterSystem,
			Map<String, Boolean> orchestrationFlags, HashMap<String, String> requestedQoS,
			HashMap<String, String> commands) {
		super();
		this.requestedService = requestedService;
		this.requesterSystem = requesterSystem;
		this.orchestrationFlags = orchestrationFlags;
		this.requestedQoS = requestedQoS;
		this.commands = commands;
	}

	public ServiceRequestForm(ArrowheadService requestedService, HashMap<String,String> requestedQoS, ArrowheadSystem requesterSystem,
		Map<String, Boolean> orchestrationFlags) {
		this.requestedService = requestedService;
		this.requestedQoS = requestedQoS;
		this.requesterSystem = requesterSystem;
		this.orchestrationFlags = orchestrationFlags;
	}

	public ArrowheadService getRequestedService() {
		return requestedService;
	}

	public void setRequestedService(ArrowheadService requestedService) {
		this.requestedService = requestedService;
	}

	public ArrowheadSystem getRequesterSystem() {
		return requesterSystem;
	}

	public void setRequesterSystem(ArrowheadSystem requesterSystem) {
		this.requesterSystem = requesterSystem;
	}

	public Map<String, Boolean> getOrchestrationFlags() {
		return orchestrationFlags;
	}

	public void setOrchestrationFlags(Map<String, Boolean> orchestrationFlags) {
		this.orchestrationFlags = orchestrationFlags;
	}

	public HashMap<String, String> getRequestedQoS() {
		return requestedQoS;
	}

	public void setRequestedQoS(HashMap<String, String> requestedQoS) {
		this.requestedQoS = requestedQoS;
	}

	public HashMap<String, String> getCommands() {
		return commands;
	}

	public void setCommands(HashMap<String, String> commands) {
		this.commands = commands;
	}

}
