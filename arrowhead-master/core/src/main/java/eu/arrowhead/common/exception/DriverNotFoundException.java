package eu.arrowhead.common.exception;

/**
 *  Related to the drivers of each communication protocol.
 *  Whenever the DriverFactory doesnt recognize the type, this exception will be launched.
 * @author Paulo
 *
 */
@SuppressWarnings("serial")
public class DriverNotFoundException extends Exception {
	
	private String message;

	public DriverNotFoundException(String message) {
		super(message);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
}
