package eu.arrowhead.common.model.qos;

import java.util.Map;

/*
 * Network Device - Class MODEL
 * 	related to all nodes in the network.
 */
public class Network_DeviceDTO {
	
	private String name;
	
	private String device_model_code;
	
	private String type;
	
	private Map<String, String> capabilities;
		
	private String address;
	
	protected Network_DeviceDTO(){}

	public Network_DeviceDTO(String name, String device_model_code, String type, Map<String, String> capabilities,
			String address) {
		super();
		this.name = name;
		this.device_model_code = device_model_code;
		this.type = type;
		this.capabilities = capabilities;
		this.address = address;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDevice_model_code() {
		return device_model_code;
	}

	public void setDevice_model_code(String device_model_code) {
		this.device_model_code = device_model_code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, String> getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(Map<String, String> capabilities) {
		this.capabilities = capabilities;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
			
}