package eu.arrowhead.common.model.qos;

import java.util.Map;

import eu.arrowhead.core.qos.factories.DataRateUnitsFactory;
import eu.arrowhead.core.qos.factories.TimeUnitsFactory;

/*
 * QoS Reservations Class MODEL
 */
public class QoS_Resource_ReservationDTO {

	
	private String state;
		
	private Network_DeviceDTO consumerDevice;
	
	private Network_DeviceDTO producerDevice;
	
	private QoS_ParameterDTO requestedQoS;
	
	private Map<String,String> configuration;

	
	public QoS_Resource_ReservationDTO(){
		
	}

	public QoS_Resource_ReservationDTO(String state, Network_DeviceDTO consumerDevice, Network_DeviceDTO producerDevice,
			QoS_ParameterDTO requestedQoS, Map<String, String> configuration) {
		super();
		this.state = state;
		this.consumerDevice = consumerDevice;
		this.producerDevice = producerDevice;
		this.requestedQoS = requestedQoS;
		this.configuration = configuration;
	}

	public String getState() {
		return state;
	}

	public Network_DeviceDTO getConsumerDevice() {
		return consumerDevice;
	}

	public void setConsumerDevice(Network_DeviceDTO consumerDevice) {
		this.consumerDevice = consumerDevice;
	}

	public Network_DeviceDTO getProducerDevice() {
		return producerDevice;
	}

	public void setProducerDevice(Network_DeviceDTO producerDevice) {
		this.producerDevice = producerDevice;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public QoS_ParameterDTO getRequestedQoS() {
		return requestedQoS;
	}

	public void setRequestedQoS(QoS_ParameterDTO requestedQoS) {
		this.requestedQoS = requestedQoS;
	}

	public Map<String, String> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Map<String, String> configuration) {
		this.configuration = configuration;
	}

	public void setRequestedQoS(Map<String,String> requestedQoS){
			
		// bandwidth		
			double bandwidth_network = new DataRateUnitsFactory().getBandwidth(requestedQoS);
			if(bandwidth_network > 0 ) this.requestedQoS.setBandwidth_network(bandwidth_network);
		// time response
			double time_response = TimeUnitsFactory.getTimeResponse(requestedQoS);
			if(time_response > 0 ) this.requestedQoS.setTime_related_deadline(bandwidth_network);		
		// add here...
		
	}
	
}
