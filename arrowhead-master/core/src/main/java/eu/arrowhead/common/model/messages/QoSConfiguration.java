package eu.arrowhead.common.model.messages;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class QoSConfiguration {
	private String commands;
	
	private QoSConfiguration(){
		
	}

	public QoSConfiguration(String commands) {
		super();
		this.commands = commands;
	}

	public String getCommands() {
		return commands;
	}

	public void setCommands(String commands) {
		this.commands = commands;
	}
	
	
	
}
