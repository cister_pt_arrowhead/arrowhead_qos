package eu.arrowhead.common.model.qos;


/*
 * QoS Reservations Parameters : Class MODEL
 */
public class QoS_ParameterDTO {

	
	private double bandwidth_network;
	
	private boolean time_related_hard_real_time = false;
	
	private boolean time_related_soft_real_time = false;
	
	private boolean time_related_relative_priorities = false;
	
	private double time_related_deadline;
	
	private boolean reliability_at_least_once = false;
	
	private boolean reliability_at_most_once = false;
	
	private boolean reliability_ordered = false;
	
	private boolean reliability_prioritized = false;

	public QoS_ParameterDTO(){
		
	}
	
	public QoS_ParameterDTO(double bandwidth_network, boolean time_related_hard_real_time,
			boolean time_related_soft_real_time, boolean time_related_relative_priorities, double time_related_deadline,
			boolean reliability_at_least_once, boolean reliability_at_most_once, boolean reliability_ordered,
			boolean reliability_prioritized) {
		super();
		this.bandwidth_network = bandwidth_network;
		this.time_related_hard_real_time = time_related_hard_real_time;
		this.time_related_soft_real_time = time_related_soft_real_time;
		this.time_related_relative_priorities = time_related_relative_priorities;
		this.time_related_deadline = time_related_deadline;
		this.reliability_at_least_once = reliability_at_least_once;
		this.reliability_at_most_once = reliability_at_most_once;
		this.reliability_ordered = reliability_ordered;
		this.reliability_prioritized = reliability_prioritized;
	}

	
	public void setBandwidth_network(int bandwidth_network) {
		this.bandwidth_network = bandwidth_network;
	}

	public boolean isTime_related_hard_real_time() {
		return time_related_hard_real_time;
	}

	public void setTime_related_hard_real_time(boolean time_related_hard_real_time) {
		this.time_related_hard_real_time = time_related_hard_real_time;
	}

	public boolean isTime_related_soft_real_time() {
		return time_related_soft_real_time;
	}

	public void setTime_related_soft_real_time(boolean time_related_soft_real_time) {
		this.time_related_soft_real_time = time_related_soft_real_time;
	}

	public boolean isTime_related_relative_priorities() {
		return time_related_relative_priorities;
	}

	public void setTime_related_relative_priorities(boolean time_related_relative_priorities) {
		this.time_related_relative_priorities = time_related_relative_priorities;
	}
	
	public double getBandwidth_network() {
		return bandwidth_network;
	}


	public void setBandwidth_network(double bandwidth_network) {
		this.bandwidth_network = bandwidth_network;
	}


	public double getTime_related_deadline() {
		return time_related_deadline;
	}


	public void setTime_related_deadline(double time_related_deadline) {
		this.time_related_deadline = time_related_deadline;
	}


	public boolean isReliability_at_least_once() {
		return reliability_at_least_once;
	}

	public void setReliability_at_least_once(boolean reliability_at_least_once) {
		this.reliability_at_least_once = reliability_at_least_once;
	}

	public boolean isReliability_at_most_once() {
		return reliability_at_most_once;
	}

	public void setReliability_at_most_once(boolean reliability_at_most_once) {
		this.reliability_at_most_once = reliability_at_most_once;
	}

	public boolean isReliability_ordered() {
		return reliability_ordered;
	}

	public void setReliability_ordered(boolean reliability_ordered) {
		this.reliability_ordered = reliability_ordered;
	}

	public boolean isReliability_prioritized() {
		return reliability_prioritized;
	}

	public void setReliability_prioritized(boolean reliability_prioritized) {
		this.reliability_prioritized = reliability_prioritized;
	}
	
	
}
