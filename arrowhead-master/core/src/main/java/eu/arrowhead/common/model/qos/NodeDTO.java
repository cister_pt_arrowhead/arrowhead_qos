package eu.arrowhead.common.model.qos;

import java.util.List;
import java.util.Map;

import eu.arrowhead.common.model.ArrowheadSystem;

public class NodeDTO {

	private String device_model_code;

	private List<Network_DeviceDTO> devices;

	private Map<String, String> capabilities;

	private String type;

	private List<ArrowheadSystem> deployed_systems;

	private Map<String, String> configuration;

	protected NodeDTO() {
		super();
	}

	public NodeDTO(String device_model_code, List<Network_DeviceDTO> devices, Map<String, String> capabilities,
			String type, List<ArrowheadSystem> deployed_systems, Map<String, String> configuration) {
		super();
		this.device_model_code = device_model_code;
		this.devices = devices;
		this.capabilities = capabilities;
		this.type = type;
		this.deployed_systems = deployed_systems;
		this.configuration = configuration;
	}

	public List<ArrowheadSystem> getDeployed_systems() {
		return deployed_systems;
	}

	public void setDeployed_systems(List<ArrowheadSystem> deployed_systems) {
		this.deployed_systems = deployed_systems;
	}

	public List<Network_DeviceDTO> getDevices() {
		return devices;
	}

	public void setDevices(List<Network_DeviceDTO> devices) {
		this.devices = devices;
	}

	public String getDevice_model_code() {
		return device_model_code;
	}

	public void setDevice_model_code(String device_model_code) {
		this.device_model_code = device_model_code;
	}

	public Map<String, String> getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(Map<String, String> capabilities) {
		this.capabilities = capabilities;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, String> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Map<String, String> configuration) {
		this.configuration = configuration;
	}

}
