package eu.arrowhead.common.model.qos;


import eu.arrowhead.common.model.ArrowheadService;
import eu.arrowhead.common.model.ArrowheadSystem;

/*
 * 	Message_Stream : Class MODEL
 * 	related to all communications between network devices.
 */
public class Message_StreamDTO {

	private ArrowheadService service;

	private ArrowheadSystem consumer;

	private ArrowheadSystem provider;
	
	private QoS_Resource_ReservationDTO reservations;
	
	private String type;

	public Message_StreamDTO(){

	}


	public ArrowheadService getService() {
		return service;
	}


	public void setService(ArrowheadService service) {
		this.service = service;
	}


	public ArrowheadSystem getConsumer() {
		return consumer;
	}


	public void setConsumer(ArrowheadSystem consumer) {
		this.consumer = consumer;
	}


	public ArrowheadSystem getProvider() {
		return provider;
	}


	public void setProvider(ArrowheadSystem provider) {
		this.provider = provider;
	}


	public QoS_Resource_ReservationDTO getReservations() {
		return reservations;
	}


	public void setReservations(QoS_Resource_ReservationDTO reservations) {
		this.reservations = reservations;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}	
	
}