package eu.arrowhead.core.qos.database.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


@Entity
@Table(name="network_device")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)  
@XmlRootElement
public class Network_Device {

	@Column(name="id")
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
    @XmlTransient
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="device_model_code")
	private String device_model_code;
	
	@Column(name="type")
	private String type;
	
	@OneToOne
	private Address address;
	
	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
	private Map<String,String> capabilities;
	
	
	protected Network_Device(){
		capabilities = new HashMap<>();
	}
	
	public Network_Device(String name, String device_model_code, String type, Address address,
			Map<String, String> capabilities) {
		super();
		this.name = name;
		this.device_model_code = device_model_code;
		this.type = type;
		this.address = address;
		this.capabilities = capabilities;
	}

	public Network_Device(String name, String device_model_code, Map<String,String> capabilities, Address address, List<ArrowheadSystem> systems, String type) {
		super();
		this.name = name;
		this.device_model_code = device_model_code;
		this.capabilities = capabilities;
		this.address = address;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDevice_model_code() {
		return device_model_code;
	}

	public void setDevice_model_code(String device_model_code) {
		this.device_model_code = device_model_code;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
		
	public String getType(){
		return this.type;
	}
	
	public void setType(String Type){
		this.type = Type;
	}

	public Map<String,String> getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(Map<String,String> capabilities) {
		this.capabilities = capabilities;
	}

}

