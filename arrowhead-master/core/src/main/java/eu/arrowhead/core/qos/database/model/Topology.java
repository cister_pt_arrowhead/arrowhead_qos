package eu.arrowhead.core.qos.database.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="Topology")
@XmlRootElement
public class Topology {
	
	@Column(name="id")
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private String id;
	
	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.ALL})
	private Network_Device end_point;
	
	@Column(name="status")
	private String status;

	public Topology(Network_Device device) {
		super();
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Network_Device getEnd_point() {
		return end_point;
	}

	public void setEnd_point(Network_Device end_point) {
		this.end_point = end_point;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
