package eu.arrowhead.core.qos.database;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;

import eu.arrowhead.common.exception.DuplicateEntryException;
import eu.arrowhead.core.qos.database.model.Network_Device;
import eu.arrowhead.core.qos.database.model.QoS_Resource_Reservation;

/**
 * @author Paulo Barbosa Database Acces Object with CRUD methods on the
 *         authorization tables of the database. (These tables are found in the
 *         *.qos.database package.)
 */
public class QoSRepositoryImpl implements IQoSRepository {

	private static SessionFactory sessionFactory;
	public static final String URL = "hibernateQOS.cfg.xml";

	public QoSRepositoryImpl() {
		if (sessionFactory == null) {
			sessionFactory = new Configuration().configure(URL).buildSessionFactory();
		}
	}

	public SessionFactory getSessionFactory() {
		if (sessionFactory != null)
			return sessionFactory;
		else {
			sessionFactory = new Configuration().configure(URL).buildSessionFactory();
			return sessionFactory;
		}
	}

	/*
	 * HERE
	 */
	public List<QoS_Resource_Reservation> getNetworkDeviceQoSReservations(Network_Device provider) {
		List<QoS_Resource_Reservation> list = new ArrayList<QoS_Resource_Reservation>();

		Session session = getSessionFactory().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			/*
			 * Criteria criteria = session.createCriteria(QoS.class);
			 * criteria.add(Restrictions.eq("systemGroup", systemGroup));
			 * systemList = (List<ArrowheadSystem>) criteria.list();
			 */
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}

		return list;
	}

	public <T> T save(T object) {
		Session session = getSessionFactory().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.saveOrUpdate(object);
			transaction.commit();
		} catch (ConstraintViolationException e) {
			if (transaction != null)
				transaction.rollback();
			throw new DuplicateEntryException(
					"There is already an entry in the " + "authorization database with these parameters.");
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}

		return object;
	}

	public <T> T saveRelation(T object) {
		Session session = getSessionFactory().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.merge(object);
			transaction.commit();
		} catch (ConstraintViolationException e) {
			if (transaction != null)
				transaction.rollback();
			throw new DuplicateEntryException(
					"There is already an entry in the " + "authorization database with these parameters.");
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}

		return object;
	}

	public <T> void delete(T object) {
		Session session = getSessionFactory().openSession();
		Transaction transaction = null;

		try {
			transaction = session.beginTransaction();
			session.delete(object);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null)
				transaction.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

}