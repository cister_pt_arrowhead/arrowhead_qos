package eu.arrowhead.core.qos.interpreter;

import java.util.Map;

public interface NodeInterpreter {
	
	public void populate(Map<String,String> parameters);

}
