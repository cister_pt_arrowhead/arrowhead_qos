package eu.arrowhead.core.qos.monitor.database;

/**
 * Repository of names and keys for the MongoDatabaseManager class.
 *
 * @author Renato Ayres
 */
public class MongoDBNames {

    public static final String PROVIDER = "provider";
    public static final String CONSUMER = "consumer";
    public static final String MONITOR_TYPE = "type";
    public static final String RULES_TABLE = "Rule";
    public static final String DOCUMENT_ID = "_id";
    public static final String PROVIDER_SYSTEM_NAME = "providerSystemName";
    public static final String PROVIDER_SYSTEM_GROUP = "providerSystemGroup";
    public static final String CONSUMER_SYSTEM_NAME = "consumerSystemName";
    public static final String CONSUMER_SYSTEM_GROUP = "consumerSystemGroup";
    public static final String TIMESTAMP = "timestamp";
    public static final String SOFTREALTIME = "softRealTime";
}
