package eu.arrowhead.core.qos.algorithms;

import java.util.List;
import java.util.Map;

import eu.arrowhead.common.model.messages.QoSVerifierResponse;
import eu.arrowhead.common.model.qos.Network_DeviceDTO;
import eu.arrowhead.common.model.qos.QoS_Resource_ReservationDTO;

public interface IQoSVerifierAlgorithm {

	public QoSVerifierResponse verifyQoS(Network_DeviceDTO provierDeviceCapabilities,
List<QoS_Resource_ReservationDTO> providerDeviceQoSReservations,
Map<String,String> requestedQoS, Map<String, String> commands);
		
}
