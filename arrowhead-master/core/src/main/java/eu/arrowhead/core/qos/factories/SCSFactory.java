package eu.arrowhead.core.qos.factories;

import java.util.ArrayList;
import java.util.HashMap;

import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.common.model.qos.Network_DeviceDTO;
import eu.arrowhead.common.model.qos.NodeDTO;
import eu.arrowhead.core.qos.database.ISCSRepository;
import eu.arrowhead.core.qos.database.SCSRepositoryImpl;
import eu.arrowhead.core.qos.database.model.Address;
import eu.arrowhead.core.qos.drivers.CommunicationProtocols;

public class SCSFactory {

	private ISCSRepository repo = new SCSRepositoryImpl();
	
	
	public Network_DeviceDTO getNetworkDeviceFromSystem(ArrowheadSystem system){
		eu.arrowhead.core.qos.database.model.ArrowheadSystem systemDB = new eu.arrowhead.core.qos.database.model.ArrowheadSystem();
		return convertToDTO(repo.getNetworkDeviceFromSystem(converFromDTO(system)));
	}
	
	public NodeDTO getNodeFromSystem(ArrowheadSystem system){
		eu.arrowhead.core.qos.database.model.ArrowheadSystem systemDB = new eu.arrowhead.core.qos.database.model.ArrowheadSystem();
		return convertToDTO(repo.getNodeFromSystem(converFromDTO(system)));
	}
	
	/******************************************/
	
	protected eu.arrowhead.core.qos.database.model.ArrowheadSystem converFromDTO(ArrowheadSystem system){
		if(system == null) return null;
		
		eu.arrowhead.core.qos.database.model.ArrowheadSystem systemDB = new eu.arrowhead.core.qos.database.model.ArrowheadSystem();
		systemDB.setAuthenticationInfo(system.getAuthenticationInfo());
		systemDB.setIPAddress(system.getIPAddress());
		systemDB.setPort(system.getPort());
		systemDB.setSystemGroup(system.getSystemGroup());
		systemDB.setSystemName(system.getSystemName());
		return systemDB;
	}
	
	protected NodeDTO convertToDTO(eu.arrowhead.core.qos.database.model.Node node){
		if(node==null) return null;
		
		NodeDTO res = new NodeDTO("DEVICE_MODEL_CODE", new ArrayList<Network_DeviceDTO>(), new HashMap<String,String>(), CommunicationProtocols.FTTSE, new ArrayList<ArrowheadSystem>(), new HashMap<String,String>() );

		
		// Set Capabilities
		
		res.setDeployed_systems(QoSFactory.convertToDTO_List(node.getDeployed_systems()));
		res.setDevice_model_code(node.getDevice_model_code());
		res.setCapabilities(node.getCapabilities());
		
		
		return res;
	}
	
	protected static eu.arrowhead.common.model.qos.Network_DeviceDTO convertToDTO(
			eu.arrowhead.core.qos.database.model.Network_Device nD){
		
		eu.arrowhead.common.model.qos.Network_DeviceDTO out = new eu.arrowhead.common.model.qos.Network_DeviceDTO( nD.getName(), nD.getDevice_model_code(),
				nD.getType(), nD.getCapabilities(), nD.getAddress().getDevice());

		return out;
	}
	
	protected static eu.arrowhead.core.qos.database.model.Network_Device convertFromDTO(
			eu.arrowhead.common.model.qos.Network_DeviceDTO nD){
		
		eu.arrowhead.core.qos.database.model.Network_Device out = 
				new eu.arrowhead.core.qos.database.model.Network_Device(nD.getName(), nD.getDevice_model_code(),
						nD.getType(), new Address(nD.getAddress()), nD.getCapabilities()  );

		
		
				
		return out;
	}
	
	public <T> T save(T object){
		return repo.save(object);
	}
	

	
}