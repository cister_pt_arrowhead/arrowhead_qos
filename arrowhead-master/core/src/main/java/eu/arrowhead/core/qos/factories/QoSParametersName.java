package eu.arrowhead.core.qos.factories;

public interface QoSParametersName {
	
	public final String BANDWIDTH = "bandwidth";
	public final String RESPONSETIME = "responseTime";
	public final String DELAY = "delay";

}
