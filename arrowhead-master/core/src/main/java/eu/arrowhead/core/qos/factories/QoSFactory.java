package eu.arrowhead.core.qos.factories;

import java.util.ArrayList;
import java.util.List;

import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.common.model.qos.Network_DeviceDTO;
import eu.arrowhead.common.model.qos.QoS_Resource_ReservationDTO;
import eu.arrowhead.core.qos.database.IQoSRepository;
import eu.arrowhead.core.qos.database.QoSRepositoryImpl;
import eu.arrowhead.core.qos.database.model.Address;

public class QoSFactory {
		
	private IQoSRepository databaseManagerQOS = new QoSRepositoryImpl();

	public List<QoS_Resource_ReservationDTO> getNetworkDeviceQoSReservations(Network_DeviceDTO provider){		
		return convertToDTO(databaseManagerQOS.getNetworkDeviceQoSReservations(SCSFactory.convertFromDTO(provider)));   	
    }
	
	protected static eu.arrowhead.common.model.qos.QoS_Resource_ReservationDTO convertToDTO(
			eu.arrowhead.core.qos.database.model.QoS_Resource_Reservation nD){
		
		eu.arrowhead.common.model.qos.QoS_Resource_ReservationDTO out = new eu.arrowhead.common.model.qos.QoS_Resource_ReservationDTO();
		
		/*out.setConsumerDevice(convertToDTO(nD.getNetwork_device_consumer()));
		out.setRequestedQoS(convertToDTO(nD.getRequestedQoS()));
		out.setProducerDevice(convertToDTO(nD.getNetwork_device_producer()));*/
		out.setState(nD.getState());
		
		return out;
	}
		
	protected static List<eu.arrowhead.common.model.qos.QoS_Resource_ReservationDTO> convertToDTO(
			List<eu.arrowhead.core.qos.database.model.QoS_Resource_Reservation> nD){
		
	List<eu.arrowhead.common.model.qos.QoS_Resource_ReservationDTO> out = new  ArrayList<>();
	for(eu.arrowhead.core.qos.database.model.QoS_Resource_Reservation qRR : nD){
		eu.arrowhead.common.model.qos.QoS_Resource_ReservationDTO qrr = convertToDTO(qRR);
		out.add(qrr);
	}
		
		return out;
	}
	
	protected static eu.arrowhead.core.qos.database.model.QoS_Resource_Reservation convertFromDTO(
			eu.arrowhead.common.model.qos.QoS_Resource_ReservationDTO in){
		
		eu.arrowhead.core.qos.database.model.QoS_Resource_Reservation out = new eu.arrowhead.core.qos.database.model.QoS_Resource_Reservation();
		
		/*out.setNetwork_device_consumer(convertFromDTO(in.getConsumerDevice()));
		out.setNetwork_device_producer(convertFromDTO(in.getProducerDevice()));
		out.setRequestedQoS(convertFromDTO(in.getRequestedQoS()));*/
		out.setState(in.getState());
		
		return out;
	} 
	
	protected static List<eu.arrowhead.core.qos.database.model.QoS_Resource_Reservation> convertFromDTO(
			List<eu.arrowhead.common.model.qos.QoS_Resource_ReservationDTO> qosR){
		return null;
	} 
	
	protected static eu.arrowhead.core.qos.database.model.ArrowheadSystem convertFromDTO(
			eu.arrowhead.common.model.ArrowheadSystem in){
		
		eu.arrowhead.core.qos.database.model.ArrowheadSystem out = new eu.arrowhead.core.qos.database.model.ArrowheadSystem();

		
		out.setAuthenticationInfo(in.getAuthenticationInfo());
		out.setIPAddress(in.getIPAddress());
		out.setPort(in.getPort());
		out.setSystemGroup(in.getSystemGroup());
		out.setSystemName(in.getSystemName());
		
		return out;
	}
	
	protected static eu.arrowhead.common.model.ArrowheadSystem convertToDTO(
			eu.arrowhead.core.qos.database.model.ArrowheadSystem in){
		
		eu.arrowhead.common.model.ArrowheadSystem out = new eu.arrowhead.common.model.ArrowheadSystem();
		
		out.setAuthenticationInfo(in.getAuthenticationInfo());
		out.setIPAddress(in.getIPAddress());
		out.setPort(in.getPort());
		out.setSystemGroup(in.getSystemGroup());
		out.setSystemName(in.getSystemName());
		
		
		return out;
	}  
	
	protected static eu.arrowhead.common.model.qos.QoS_ParameterDTO convertToDTO(
			eu.arrowhead.core.qos.database.model.QoS_Parameter nD){
		
		eu.arrowhead.common.model.qos.QoS_ParameterDTO out = new eu.arrowhead.common.model.qos.QoS_ParameterDTO();
				
		out.setBandwidth_network(nD.getBandwidth_network());
		out.setReliability_at_least_once(nD.isReliability_at_least_once());
		out.setReliability_at_most_once(nD.isReliability_at_most_once());
		out.setReliability_ordered(nD.isReliability_ordered());
		out.setReliability_prioritized(nD.isReliability_prioritized());
		out.setTime_related_deadline(nD.getTime_related_deadline());
		out.setTime_related_hard_real_time(nD.isTime_related_hard_real_time());
		out.setTime_related_relative_priorities(nD.isTime_related_relative_priorities());
		out.setTime_related_soft_real_time(nD.isTime_related_soft_real_time());
		
		
		return out;
	}
	
	protected static eu.arrowhead.core.qos.database.model.QoS_Parameter  convertFromDTO(
			eu.arrowhead.common.model.qos.QoS_ParameterDTO nD){
		
		eu.arrowhead.core.qos.database.model.QoS_Parameter out = new eu.arrowhead.core.qos.database.model.QoS_Parameter();
		
		out.setBandwidth_network(nD.getBandwidth_network());
		out.setReliability_at_least_once(nD.isReliability_at_least_once());
		out.setReliability_at_most_once(nD.isReliability_at_most_once());
		out.setReliability_ordered(nD.isReliability_ordered());
		out.setReliability_prioritized(nD.isReliability_prioritized());
		out.setTime_related_deadline(nD.getTime_related_deadline());
		out.setTime_related_hard_real_time(nD.isTime_related_hard_real_time());
		out.setTime_related_relative_priorities(nD.isTime_related_relative_priorities());
		out.setTime_related_soft_real_time(nD.isTime_related_soft_real_time());
		
		return out;
	}
	
	protected static List<ArrowheadSystem> convertToDTO_List( List<eu.arrowhead.core.qos.database.model.ArrowheadSystem> in){
		if(in==null) return null;
		List<ArrowheadSystem> out = new ArrayList<>();
		for(eu.arrowhead.core.qos.database.model.ArrowheadSystem system : in){
			out.add(convertToDTO(system));
		}
		
		return out;
	}
	
	protected static List<eu.arrowhead.core.qos.database.model.ArrowheadSystem> convertFromDTO_List( List<ArrowheadSystem> in){
		if(in==null) return null;
		List< eu.arrowhead.core.qos.database.model.ArrowheadSystem> out = new ArrayList<>();
		for(ArrowheadSystem system : in){
			out.add(convertFromDTO(system));
		}
		
		return out;
	}
	
	public <T> T save(T object){
		 return databaseManagerQOS.save(object);
	 }

}