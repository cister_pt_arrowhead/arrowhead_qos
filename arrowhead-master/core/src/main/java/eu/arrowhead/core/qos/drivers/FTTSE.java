package eu.arrowhead.core.qos.drivers;

import java.net.URI;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import eu.arrowhead.common.model.ArrowheadService;
import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.common.model.messages.QoSReservationCommand;
import eu.arrowhead.common.model.qos.NodeDTO;

public class FTTSE extends QoSDriver{

	private final String MIDDLEWARE_IP = "localhost";
	private final String MIDDLEWARE_URL = "/RESTful/manager/configure";

	protected FTTSE(){
		
	}
	
	public Boolean configure(Map<String,String> qos, ArrowheadSystem producer,ArrowheadSystem consumer, NodeDTO producerN, NodeDTO consumerN, Map<String, String> commands, ArrowheadService service){
		// Contactar o middlware - fazer um cliente RESTful em que 
		//envia um QoSReservationCommand
		Client client = ClientBuilder.newClient();
		URI uri = UriBuilder.fromPath("http://" + MIDDLEWARE_IP + ":" + "8080" + MIDDLEWARE_URL).build();

		WebTarget target = client.target(uri);
		Response response = target.request().header("Content-type", "application/json")
				.post(Entity.json(new QoSReservationCommand(service, producer, consumer, commands, qos)));
		
		if(response.getStatus() > 199 && response.getStatus() < 300 ){
			return true;
		}
		
		return false;
	}
	

}
