package eu.arrowhead.core.qos.drivers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.arrowhead.common.exception.DriverNotFoundException;
import eu.arrowhead.common.exception.ReservationException;
import eu.arrowhead.common.model.ArrowheadService;
import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.common.model.qos.Network_DeviceDTO;
import eu.arrowhead.common.model.qos.NodeDTO;
import eu.arrowhead.core.qos.factories.DataRateUnitsFactory;
import eu.arrowhead.core.qos.factories.TimeUnitsFactory;

public class DriversFactory {

	private static DriversFactory instance;
	private static HashMap<String, QoSDriver> driversClasses;

	private DriversFactory() {
		//
	}

	public static DriversFactory getInstance() {
		if (instance == null) {
			instance = new DriversFactory();
		}
		bootstrap();
		return instance;
	}

	/**
	 * You add the drivers here!
	 */
	private static void bootstrap() {
		if (driversClasses == null)
			driversClasses = new HashMap<>();
		driversClasses.put(CommunicationProtocols.FTTSE, (new FTTSE()));
	}

	private QoSDriver getDriver(String type) {
		return driversClasses.get(type);
	}

	/**
	 * 
	 * @param driver
	 * @param specification
	 * @return
	 * @throws DriverNotFoundException 
	 */
	public String generateCommands(Map<String, String> requestedQoS, ArrowheadSystem producer, ArrowheadSystem consumer,
			NodeDTO producerNode, NodeDTO consumerNode, Map<String, String> commands, ArrowheadService service)
			throws ReservationException, DriverNotFoundException {

		if (consumerNode.getType().compareToIgnoreCase(producerNode.getType()) != 0) {
			// Excepção!
			return null;
		}

		String type = consumerNode.getType();
		QoSDriver driver = getDriver(type);

		if (driver == null) {
			// Excepção!
			throw new DriverNotFoundException("Driver Unrecognized!");
		}

		Boolean response = driver.configure(requestedQoS, producer, consumer , producerNode, consumerNode, commands,
				service);
		
		if(!response) throw new ReservationException();

		return null;

	}

	private String gatherCommands(List<String> commands) {
		return commands.get(0);
	}

}