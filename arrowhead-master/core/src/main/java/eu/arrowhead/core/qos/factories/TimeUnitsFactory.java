package eu.arrowhead.core.qos.factories;

import java.util.Map;

public class TimeUnitsFactory {
	
	public static double getTimeResponse(Map<String,String> requestedQoS){
		String temp = requestedQoS.get(QoSParameters.TIME_RESPONSE);
		if(temp==null) return -1;
		
		try{
			double value =  Double.parseDouble(requestedQoS.get(QoSParameters.TIME_RESPONSE));
			return value;
		}catch(NumberFormatException | NullPointerException ex){
			return -1;
		}
	}	
}
