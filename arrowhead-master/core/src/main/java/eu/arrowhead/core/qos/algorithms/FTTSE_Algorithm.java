package eu.arrowhead.core.qos.algorithms;

import java.util.List;
import java.util.Map;

import eu.arrowhead.common.model.messages.QoSVerifierResponse;
import eu.arrowhead.common.model.qos.Network_DeviceDTO;
import eu.arrowhead.common.model.qos.QoS_Resource_ReservationDTO;

public class FTTSE_Algorithm implements IQoSVerifierAlgorithm{

	public FTTSE_Algorithm(){
		
	}
	
	@Override
	public QoSVerifierResponse verifyQoS(Network_DeviceDTO provierDeviceCapabilities,
			List<QoS_Resource_ReservationDTO> providerDeviceQoSReservations,
			Map<String,String> requestedQoS, Map<String, String> commands) {
		
		QoSVerifierResponse response = new QoSVerifierResponse();
		response.setResponse(true);
		
		return response;
	}
	
}
