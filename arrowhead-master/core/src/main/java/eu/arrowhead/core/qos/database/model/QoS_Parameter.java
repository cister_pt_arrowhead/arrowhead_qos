package eu.arrowhead.core.qos.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="QOS_PARAMETER")
@XmlRootElement
public class QoS_Parameter {

	@Id 
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="bandwidth_network")
	private double bandwidth_network;
	
	@Column(name="time_related_hard_real_time")
	private boolean time_related_hard_real_time;
	
	@Column(name="time_related_soft_real_time")
	private boolean time_related_soft_real_time;
	
	@Column(name="time_related_relative_priorities")
	private boolean time_related_relative_priorities;
	
	@Column(name="time_related_deadline")
	private double time_related_deadline;
	
	@Column(name="reliability_at_least_once")
	private boolean reliability_at_least_once;
	
	@Column(name="reliability_at_most_once")
	private boolean reliability_at_most_once;
	
	@Column(name="reliability_ordered")
	private boolean reliability_ordered;
	
	@Column(name="reliability_prioritized")
	private boolean reliability_prioritized;

	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}



	public double getBandwidth_network() {
		return bandwidth_network;
	}

	public void setBandwidth_network(double bandwidth_network) {
		this.bandwidth_network = bandwidth_network;
	}

	public boolean isTime_related_hard_real_time() {
		return time_related_hard_real_time;
	}

	public void setTime_related_hard_real_time(boolean time_related_hard_real_time) {
		this.time_related_hard_real_time = time_related_hard_real_time;
	}

	public boolean isTime_related_soft_real_time() {
		return time_related_soft_real_time;
	}

	public void setTime_related_soft_real_time(boolean time_related_soft_real_time) {
		this.time_related_soft_real_time = time_related_soft_real_time;
	}

	public boolean isTime_related_relative_priorities() {
		return time_related_relative_priorities;
	}

	public void setTime_related_relative_priorities(boolean time_related_relative_priorities) {
		this.time_related_relative_priorities = time_related_relative_priorities;
	}

	public double getTime_related_deadline() {
		return time_related_deadline;
	}

	public void setTime_related_deadline(double time_related_deadline) {
		this.time_related_deadline = time_related_deadline;
	}

	public boolean isReliability_at_least_once() {
		return reliability_at_least_once;
	}

	public void setReliability_at_least_once(boolean reliability_at_least_once) {
		this.reliability_at_least_once = reliability_at_least_once;
	}

	public boolean isReliability_at_most_once() {
		return reliability_at_most_once;
	}

	public void setReliability_at_most_once(boolean reliability_at_most_once) {
		this.reliability_at_most_once = reliability_at_most_once;
	}

	public boolean isReliability_ordered() {
		return reliability_ordered;
	}

	public void setReliability_ordered(boolean reliability_ordered) {
		this.reliability_ordered = reliability_ordered;
	}

	public boolean isReliability_prioritized() {
		return reliability_prioritized;
	}

	public void setReliability_prioritized(boolean reliability_prioritized) {
		this.reliability_prioritized = reliability_prioritized;
	}
	
	
	
}
