package eu.arrowhead.core.qos.interpreters.stream;

import java.util.Map;

import eu.arrowhead.core.qos.interpreter.StreamInterpreter;

public class FTTSE implements StreamInterpreter {

	private final String ID_I = "ID";
	private int id;

	private final String SIZE_D = "SIZE";
	private double size;

	private final String PERIOD_D = "PERIOD";
	private double period;

	private final String SYNCHRONOUS = "SYNCHRONOUS";
	private boolean synchronous;

	/*
	 * Maximum Transmission Unit
	 */
	private final String MTU = "MTU";
	private double mtu;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public double getPeriod() {
		return period;
	}

	public void setPeriod(double period) {
		this.period = period;
	}

	public boolean isSynchronous() {
		return synchronous;
	}

	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}

	public double getMtu() {
		return mtu;
	}

	public void setMtu(double mtu) {
		this.mtu = mtu;
	}

	public String getID_I() {
		return ID_I;
	}

	public String getSIZE_D() {
		return SIZE_D;
	}

	public String getPERIOD_D() {
		return PERIOD_D;
	}

	public String getSYNCHRONOUS() {
		return SYNCHRONOUS;
	}

	public String getMTU() {
		return MTU;
	}

	@Override
	public void populate(Map<String, String> paramters) {
		// TODO Auto-generated method stub

	}

}
