package eu.arrowhead.core.qos.database;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;

import eu.arrowhead.common.exception.DuplicateEntryException;
import eu.arrowhead.core.qos.database.model.ArrowheadSystem;
import eu.arrowhead.core.qos.database.model.Network_Device;
import eu.arrowhead.core.qos.database.model.Node;


public class SCSRepositoryImpl implements ISCSRepository{
	
    private static SessionFactory sessionFactory;
    public static final String URL = "hibernateSCS.cfg.xml";
   
    public SCSRepositoryImpl(){
        if (sessionFactory == null){
            sessionFactory = new Configuration().configure(URL).buildSessionFactory();
        }
    }  
   
    public SessionFactory getSessionFactory() {
    	if (sessionFactory != null)
    	return sessionFactory;
    	else {
    		sessionFactory = new Configuration().configure(URL).buildSessionFactory();
    		return sessionFactory;
    	}
    }
  
    public Node getNodeFromSystem(ArrowheadSystem provider){
    	return null;
    }
    
	public Network_Device getNetworkDeviceFromSystem(ArrowheadSystem provider){
		return null;
	}
     
    public <T> T save(T object){
		Session session = getSessionFactory().openSession();
    	Transaction transaction = null;
    	
    	try {
    		transaction = session.beginTransaction();
    		session.saveOrUpdate(object);
            transaction.commit();
        }
    	catch(ConstraintViolationException e){
    		if (transaction!=null) transaction.rollback();
    		throw new DuplicateEntryException("There is already an entry in the "
    				+ "authorization database with these parameters.");
    	}
        catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            throw e;
        }
        finally {
            session.close();
        }
    	
    	return object;	
	}
    
    public <T> T saveRelation(T object){
		Session session = getSessionFactory().openSession();
    	Transaction transaction = null;
    	
    	try {
    		transaction = session.beginTransaction();
    		session.merge(object);
            transaction.commit();
        }
    	catch(ConstraintViolationException e){
    		if (transaction!=null) transaction.rollback();
    		throw new DuplicateEntryException("There is already an entry in the "
    				+ "authorization database with these parameters.");
    	}
        catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            throw e;
        }
        finally {
            session.close();
        }
    	
    	return object;	
	}
    
	public <T> void delete(T object){
		Session session = getSessionFactory().openSession();
    	Transaction transaction = null;
    	
    	try {
    		transaction = session.beginTransaction();
    		session.delete(object);
            transaction.commit();
        }
        catch (Exception e) {
            if (transaction!=null) transaction.rollback();
            throw e;
        }
        finally {
            session.close();
        }	
	}
  
	
}