package eu.arrowhead.core.serviceregistry;

import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import eu.arrowhead.common.configuration.SysConfig;
import eu.arrowhead.common.model.messages.ServiceQueryForm;
import eu.arrowhead.common.model.messages.ServiceQueryResult;
import eu.arrowhead.common.model.messages.ServiceRegistryEntry;

@Path("serviceregistry")
public class ServiceRegistryResource {
	
	private String registry_IP = "arrowhead2.tmit.bme.hu";
	public SysConfig sysConfig = SysConfig.getInstance();


	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getIt() {
		return "This is the Service Registry.";
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{serviceGroup}/{service}/{interf}")
	public void publishingToRegistry(@PathParam("serviceGroup") String serviceGroup, @PathParam("service") String service,
			@PathParam("interf") String interf, ServiceRegistryEntry entry) {
		
		//ServiceRegistry.getInstance().register(serviceGroup, service, interf, entry);
		
		Response response = redirectToAnotherSR_POST(serviceGroup, service, interf, entry);				
		
		// chamar o authorization caso se tenha obtido um response de sucesso
		int status = response.getStatus();
		if(status > 199 && status < 300){
			addAuth(serviceGroup, service, interf, entry);
		}else{
			throw new InternalServerErrorException("Erro no registo do serviço!");
		}
	}
	
	/**
	 * 
	 * @param serviceGroup
	 * @param service
	 * @param interf
	 * @param entry
	 */
	public Response addAuth(String serviceGroup, String service,
 String interf, ServiceRegistryEntry entry){
		
		Client client = ClientBuilder.newClient();
		String a = sysConfig.getAuthorizationURI();
		URI uri = UriBuilder.fromPath(sysConfig.getAuthorizationURI()).path("addProviderToService").
				path(serviceGroup).path(service).path(interf).build();
		
		WebTarget target = client.target(uri);
		Response response = target.request().header("Content-type", "application/json")
				.post(Entity.json(entry));	
		return response;
	}
	
	/**
	 * 
	 * @param serviceGroup
	 * @param service
	 * @param interf
	 * @param entry
	 */
	public Response redirectToAnotherSR_POST(String serviceGroup, String service,
 String interf, ServiceRegistryEntry entry){
		
		Client client = ClientBuilder.newClient();
		URI uri = UriBuilder.fromPath("http://" + registry_IP + ":" + "8080").path("core").path("serviceregistry")
				.path(serviceGroup).path(service).path(interf).build();
		
		WebTarget target = client.target(uri);
		Response response = target.request().header("Content-type", "application/json")
				.post(Entity.json(entry));	
		return response;
	}
	
	/**
	 * 
	 * @param serviceGroup
	 * @param service
	 * @param interf
	 * @param entry
	 */
	public Response redirectToAnotherSR_DELETE(String serviceGroup, String service,
 String interf, ServiceRegistryEntry entry){
		
		Client client = ClientBuilder.newClient();
		URI uri = UriBuilder.fromPath("http://" + registry_IP + ":" + "8080").path("core").path("serviceregistry")
				.path(serviceGroup).path(service).path(interf).build();
		
		WebTarget target = client.target(uri);
		Response response = target.request("application/json").build("DELETE", Entity.
				  json(entry)).invoke();
				
		return response;
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/{serviceGroup}/{service}/{interf}")
	public void removingFromRegistry(@PathParam("serviceGroup") String serviceGroup, @PathParam("service") String service,
			@PathParam("interf") String interf, ServiceRegistryEntry entry) {
		//ServiceRegistry.getInstance().unRegister(serviceGroup, service, interf, entry);		
		redirectToAnotherSR_DELETE(serviceGroup, service, interf, entry);	
	}
	

	/**
	 * Query paramaters are the fields from Service
	 * 
	 * Query Form GET-be query parameternek egy string lista kell IDD-k
	 * 
	 * @param serviceGroup
	 * @param service
	 * @param interf
	 * @param serviceMetadata
	 * @param pingProviders
	 * @return Providers: List of (ArrowheadSystem, ServiceURI:String)
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value = "/{serviceGroup}/{service}")
	public ServiceQueryResult getServiceQueryForm(@PathParam("serviceGroup") String serviceGroup,
			@PathParam("service") String service, ServiceQueryForm queryForm) {
				
		//return ServiceRegistry.getInstance().provideServices(serviceGroup, service, queryForm);
		return redirectToAnotherSR_PUT(serviceGroup, service, queryForm );
	}
	
	/**
	 * 
	 * @param serviceGroup
	 * @param service
	 * @param queryForm
	 * @return
	 */
	public ServiceQueryResult redirectToAnotherSR_PUT(String serviceGroup, String service, ServiceQueryForm queryForm){
		
		Client client = ClientBuilder.newClient();
		URI uri = UriBuilder.fromPath("http://" + registry_IP + ":" + "8080").path("core").path("serviceregistry")
				.path(serviceGroup).path(service).build();
		
		WebTarget target = client.target(uri);
		Response response = target.request().header("Content-type", "application/json")
				.put(Entity.json(queryForm));	
		
		return response.readEntity(ServiceQueryResult.class);
		
	}
}
