package eu.arrowhead.core.qos.drivers;

import java.util.Map;

import eu.arrowhead.common.exception.QoSParamException;
import eu.arrowhead.common.model.ArrowheadService;
import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.common.model.qos.NodeDTO;

public abstract class QoSDriver {
		
	public QoSDriver(){
		//
	}
	
	public Boolean configure(Map<String,String> qos, ArrowheadSystem producer,
			ArrowheadSystem consumer, NodeDTO producerN,
			NodeDTO consumerN, Map<String, String> commands
			, ArrowheadService service){
		return false;
	}
	
	private String bandwidth(Double requestedValue) throws QoSParamException{
		throw new QoSParamException("NOT IMPLEMENTED");
	}
	
	private String time_response(Double requestedValue) throws QoSParamException{
		throw new QoSParamException("NOT IMPLEMENTED");
	}
	
	private String delay(Double requestedValue)  throws QoSParamException{
		throw new QoSParamException("NOT IMPLEMENTED");
	}
	
	
	
}
