package eu.arrowhead.core.qos.database.model;


import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="capability", uniqueConstraints={@UniqueConstraint(columnNames={"consumer_id", "provider_id", "service_id"}, name="UKnl06duvgwnau3mkekw90hctba")})
@XmlRootElement
public class Message_Stream {

	@Column(name="id")
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;

	@JoinColumn(name="service_id")
	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.ALL})
	private ArrowheadService service;

	@JoinColumn(name="consumer_id")
	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.ALL})
	private ArrowheadSystem consumer;

	@JoinColumn(name="provider_id")
	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.ALL})
	private ArrowheadSystem provider;
	
	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.ALL})
	private QoS_Resource_Reservation reservation;
	
	@Column(name="configuration")
	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
	private Map<String, String> configuration;
	
	private String type;
	
	
	
	public Message_Stream(){

	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public ArrowheadService getService() {
		return service;
	}


	public void setService(ArrowheadService service) {
		this.service = service;
	}


	public ArrowheadSystem getConsumer() {
		return consumer;
	}


	public void setConsumer(ArrowheadSystem consumer) {
		this.consumer = consumer;
	}

	public ArrowheadSystem getProvider() {
		return provider;
	}

	public void setProvider(ArrowheadSystem provider) {
		this.provider = provider;
	}
	
	public QoS_Resource_Reservation getReservation() {
		return reservation;
	}

	public void setReservation(QoS_Resource_Reservation reservation) {
		this.reservation = reservation;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
