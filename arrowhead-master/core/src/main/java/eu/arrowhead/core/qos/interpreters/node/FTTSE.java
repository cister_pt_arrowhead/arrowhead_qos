package eu.arrowhead.core.qos.interpreters.node;

import java.util.Map;

import eu.arrowhead.core.qos.interpreter.NodeInterpreter;

public class FTTSE implements NodeInterpreter{
	
	public static final String APPID_I = "APPID";
	private int AppID;
	
	public static final String EC_I = "EC";
	private int elementary_cycle;
	
	public static final String MASTER_B = "MASTER";
	private boolean master;
	
	protected FTTSE(){
		super();
	}

	public FTTSE(int appID, int elementary_cycle, boolean master) {
		super();
		AppID = appID;
		this.elementary_cycle = elementary_cycle;
		this.master = master;
	}
	
	public FTTSE(Map<String, String> parameters){
		setAppID(parameters.get(APPID_I));
		setElementary_cycle(parameters.get(EC_I));
		setMaster(parameters.get(MASTER_B));
	}

	public int getElementary_cycle() {
		return elementary_cycle;
	}

	public void setElementary_cycle(int elementary_cycle) {
		this.elementary_cycle = elementary_cycle;
	}

	public boolean isMaster() {
		return master;
	}

	public void setMaster(boolean master) {
		this.master = master;
	}

	public int getAppID() {
		return AppID;
	}

	public void setAppID(int appID) {
		AppID = appID;
	}
	
	public void setAppID(String appid) {
		try{
			AppID = Integer.parseInt( appid );
		} catch(IllegalArgumentException nfe){
			return;
		}
	}
	
	public void setMaster(String master) {
		try{
			this.master = Boolean.parseBoolean(master);
		} catch(IllegalArgumentException nfe){
			return;
		}
	}
	
	public void setElementary_cycle(String elementary_cycle) {
		try{
			this.elementary_cycle = Integer.parseInt( elementary_cycle );
		} catch(IllegalArgumentException nfe){
			return;
		}
	}

	public void populate(Map<String, String> paramters) {
		// TODO Auto-generated method stub
		
	}

	
	
}
