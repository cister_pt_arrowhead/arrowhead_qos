package eu.arrowhead.core.qos.algorithms;

import java.util.HashMap;
import java.util.Map;

import eu.arrowhead.core.qos.drivers.CommunicationProtocols;

public class VerifierAlgorithmFactory{
	
	private static VerifierAlgorithmFactory instance;
	private static Map<String,IQoSVerifierAlgorithm> list;

	private VerifierAlgorithmFactory(){

	}
	
	public static VerifierAlgorithmFactory getInstance(){
		if(instance==null){
			instance = new VerifierAlgorithmFactory();
		}
		bootstrap();
		return instance;
	}
	
	public IQoSVerifierAlgorithm getAlgorithm(String type){
		if(type.equalsIgnoreCase(CommunicationProtocols.FTTSE)){
			return new FTTSE_Algorithm();
		}
		
		// manda exceção
		return null;
	}
	
	private static void bootstrap(){
		if(list==null) list = new HashMap<>();
		
		list.put(CommunicationProtocols.FTTSE, new FTTSE_Algorithm());
		
	}

}
