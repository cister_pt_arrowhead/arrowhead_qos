package eu.arrowhead.core.qos.database;

import java.util.List;

import eu.arrowhead.core.qos.database.model.Network_Device;
import eu.arrowhead.core.qos.database.model.QoS_Resource_Reservation;

public interface IQoSRepository {

	public List<QoS_Resource_Reservation> getNetworkDeviceQoSReservations(Network_Device provider);
	
	public <T> T save(T object);
	 
}