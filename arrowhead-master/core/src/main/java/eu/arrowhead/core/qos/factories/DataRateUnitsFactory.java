package eu.arrowhead.core.qos.factories;

import java.util.Map;


/**
 *  SI Units. 
 *  https://en.wikipedia.org/wiki/Data_rate_units
 * @author Paulo
 *
 */
public class DataRateUnitsFactory {
	
	/**
	 * It will return the value in Mbit/s.
	 * @param specifications
	 * @return
	 */
	public double getBandwidth(Map<String,String> requestedQoS){
		String temp = requestedQoS.get(QoSParameters.BANDWIDTH);
		if(temp==null) return -1;
		
		try{
			double value =  Double.parseDouble(requestedQoS.get(QoSParameters.BANDWIDTH));
			return value;
		}catch(NumberFormatException | NullPointerException ex){
			return -1;
		}
		
	}
	

	
}