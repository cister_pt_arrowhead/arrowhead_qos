package eu.arrowhead.core.qos.interpreters.node;

import java.util.HashMap;
import java.util.Map;

import eu.arrowhead.core.qos.drivers.CommunicationProtocols;
import eu.arrowhead.core.qos.interpreter.NodeInterpreter;

public class NodeFactory {
	
	private static NodeFactory instance;
	private static Map<String, NodeInterpreter> list;
	
	private NodeFactory(){
		bootstrap();
	}
	
	public static NodeFactory getInstance(){
		if(instance == null){
			instance = new NodeFactory();
		}
		return instance;
	}
	
	private static void bootstrap(){
		if(list == null) list = new HashMap<>();
		
		list.put(CommunicationProtocols.FTTSE, new FTTSE());
	}

}

