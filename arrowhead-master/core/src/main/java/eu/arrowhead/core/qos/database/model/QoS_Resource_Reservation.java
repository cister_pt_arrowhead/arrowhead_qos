package eu.arrowhead.core.qos.database.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name="QOS_RESOURCE_RESERVATION")
@XmlRootElement
public class QoS_Resource_Reservation {
	
	@Column(name="id")
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="state")
	private String state;
	
	@JoinColumn(name="stream_id")
	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.ALL})	
	private Message_Stream stream;
	
	@JoinColumn(name="parameter_id")
	@OneToOne(fetch = FetchType.EAGER, cascade={CascadeType.ALL})	
	private QoS_Parameter parameter;
	
	public QoS_Resource_Reservation(){
		
	}
	
	public QoS_Resource_Reservation(String state) {
		super();
		this.state = state;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Message_Stream getStream() {
		return stream;
	}

	public void setStream(Message_Stream stream) {
		this.stream = stream;
	}

	public QoS_Parameter getParameter() {
		return parameter;
	}

	public void setParameter(QoS_Parameter parameter) {
		this.parameter = parameter;
	}


}