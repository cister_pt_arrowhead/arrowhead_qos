package eu.arrowhead.core.qos.factories;


public interface QoSParameters {
	
	public static final String BANDWIDTH = "bandwidth";
	public static final String TIME_RESPONSE = "time-response";
	
}
