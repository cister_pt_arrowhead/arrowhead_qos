package eu.arrowhead.core.qos.interpreter;

import java.util.Map;

public interface StreamInterpreter {
	
	public void populate(Map<String,String> parameters);

}
