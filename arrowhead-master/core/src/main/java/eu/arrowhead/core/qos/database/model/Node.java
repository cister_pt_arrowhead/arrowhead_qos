package eu.arrowhead.core.qos.database.model;

import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name="NODE")
@XmlRootElement
public class Node {
	
	@Column(name="id")
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
    @XmlTransient
	private int id;
	
	@Column(name="device_model_code")
	private String device_model_code;
	
	@OneToMany
	private List<Network_Device> devices;
	
	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
	private Map<String, String> capabilities;
	
	private String type;
	
	@OneToMany
	private List<ArrowheadSystem> deployed_systems;
	
	@Column(name="configuration")
	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
	private Map<String, String> configuration;
	
	
	protected Node(){
		
	}
	

	public Node(String device_model_code, List<Network_Device> devices, Map<String, String> capabilities, String type,
			List<ArrowheadSystem> deployed_systems, Map<String, String> configuration) {
		super();
		this.device_model_code = device_model_code;
		this.devices = devices;
		this.capabilities = capabilities;
		this.type = type;
		this.deployed_systems = deployed_systems;
		this.configuration = configuration;
	}


	public String getDevice_model_code() {
		return device_model_code;
	}

	public void setDevice_model_code(String device_model_code) {
		this.device_model_code = device_model_code;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Network_Device> getDevices() {
		return devices;
	}

	public void setDevices(List<Network_Device> devices) {
		this.devices = devices;
	}
	
	public Map<String, String> getCapabilities() {
		return capabilities;
	}

	public void setCapabilities(Map<String, String> capabilities) {
		this.capabilities = capabilities;
	}

	public List<ArrowheadSystem> getDeployed_systems() {
		return deployed_systems;
	}

	public void setDeployed_systems(List<ArrowheadSystem> deployed_systems) {
		this.deployed_systems = deployed_systems;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Map<String, String> getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Map<String, String> configuration) {
		this.configuration = configuration;
	}

		
}