package eu.arrowhead.core.qos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import eu.arrowhead.common.configuration.SysConfig;
import eu.arrowhead.common.exception.DriverNotFoundException;
import eu.arrowhead.common.exception.ReservationException;
import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.common.model.messages.QoSConfiguration;
import eu.arrowhead.common.model.messages.QoSMonitorAddRule;
import eu.arrowhead.common.model.messages.QoSReservationCommand;
import eu.arrowhead.common.model.messages.QoSReservationResponse;
import eu.arrowhead.common.model.messages.QoSReserve;
import eu.arrowhead.common.model.messages.QoSVerificationResponse;
import eu.arrowhead.common.model.messages.QoSVerifierResponse;
import eu.arrowhead.common.model.messages.QoSVerify;
import eu.arrowhead.common.model.qos.Message_StreamDTO;
import eu.arrowhead.common.model.qos.Network_DeviceDTO;
import eu.arrowhead.common.model.qos.NodeDTO;
import eu.arrowhead.common.model.qos.QoS_Resource_ReservationDTO;
import eu.arrowhead.core.qos.algorithms.IQoSVerifierAlgorithm;
import eu.arrowhead.core.qos.algorithms.VerifierAlgorithmFactory;
import eu.arrowhead.core.qos.drivers.CommunicationProtocols;
import eu.arrowhead.core.qos.drivers.DriversFactory;
import eu.arrowhead.core.qos.factories.QoSFactory;
import eu.arrowhead.core.qos.factories.SCSFactory;

public class QoSService {

	private static Logger log = Logger.getLogger(QoSService.class.getName());
	private VerifierAlgorithmFactory algorithmFactory;

	private SCSFactory scsfactory;
	private QoSFactory qosfactory;

	public SysConfig sysConfig = SysConfig.getInstance();

	private Client client;

	public QoSService() {
		algorithmFactory = VerifierAlgorithmFactory.getInstance();
		qosfactory = new QoSFactory();
		scsfactory = new SCSFactory();
		client = ClientBuilder.newClient();

	}

	/*
	 * 
	 */
	public QoSVerificationResponse qoSVerify(QoSVerify message) {
		QoSVerificationResponse qosVerificationResponse = new QoSVerificationResponse();
		log.info("QoS: Verifying QoS paramteres.");

		for (ArrowheadSystem system : message.getProvider()) {
			// Get Provider Network
			Network_DeviceDTO provider_network_device = scsfactory.getNetworkDeviceFromSystem(system);

			// Get Provider Arrowhead System
			List<QoS_Resource_ReservationDTO> providerDeviceQoSReservations = getNetworkDeviceQoSReservations(
					provider_network_device);

			// Run Algorithm
			IQoSVerifierAlgorithm algorithm = algorithmFactory.getAlgorithm(provider_network_device.getType());
			QoSVerifierResponse response = algorithm.verifyQoS(provider_network_device, providerDeviceQoSReservations,
					message.getRequestedQoS(), message.getCommands());

			updateQoSVerificationResponse(system, response, qosVerificationResponse);
		}

		log.info("QoS: QoS paramteres verified.");
		return qosVerificationResponse;
	}

	public QoSReservationResponse qoSReserve(QoSReserve message) {
		QoSReservationResponse qosreservationResponse;

		/******************* Get Nodes! *********************/
		// Go To System Configuration Store get NetworkDevice
		ArrowheadSystem consumer = message.getConsumer();
		ArrowheadSystem provider = message.getProvider();
		HashMap<String, String> qos = (HashMap<String, String>) message.getRequestedQoS();

		/*Network_DeviceDTO cDevice = scsfactory.getNetworkDeviceFromSystem(consumer);
		Network_DeviceDTO pDevice = scsfactory.getNetworkDeviceFromSystem(provider);*/

		/*NodeDTO cNode = scsfactory.getNodeFromSystem(consumer);
		NodeDTO pNode = scsfactory.getNodeFromSystem(provider);*/
		NodeDTO cNode = new NodeDTO("DEVICE_MODEL_CODE", new ArrayList<Network_DeviceDTO>(), new HashMap<String,String>(), CommunicationProtocols.FTTSE, new ArrayList<ArrowheadSystem>(), new HashMap<String,String>() );
		NodeDTO pNode = new NodeDTO("DEVICE_MODEL_CODE", new ArrayList<Network_DeviceDTO>(), new HashMap<String,String>(), CommunicationProtocols.FTTSE, new ArrayList<ArrowheadSystem>(), new HashMap<String,String>() );

		/*************************************************************/

		/******************* Generate Commands *********************/

		
		
		/* for consumer */
		try {
			String responseS = DriversFactory.getInstance().generateCommands(message.getRequestedQoS(), provider,
					consumer, pNode, cNode, message.getCommands(), message.getService());
		} catch (ReservationException e) {

		} catch (DriverNotFoundException e) {

		}

		/************************************************************/

		/******
		 * IF SUCCESS: Saving Quality of Service Reservation on DataBase
		 ***/
		/* Create Message Stream */
		Message_StreamDTO messageStream = new Message_StreamDTO();
		messageStream.setConsumer(consumer);
		messageStream.setProvider(provider);
		messageStream.setService(message.getService());

		/* Make Reservation And Record */
		/*QoS_Resource_ReservationDTO reservation = new QoS_Resource_ReservationDTO();
		reservation.setConsumerDevice(cDevice);
		reservation.setProducerDevice(pDevice);
		reservation.setRequestedQoS(qos);

		messageStream.setReservations(reservation);
		qosfactory.save(messageStream);*/
		/*********************************************/

		/****** IF SUCCESS: Contact Monitoring Core System *******/
		// Send to Monitor
		QoSMonitorAddRule rule = new QoSMonitorAddRule();
		/*
		 * rule.setConsumer(consumer); rule.setOverwrite(overwrite);
		 * rule.setParameters(parameters); rule.setProvider(provider);
		 * rule.setType(messageStream.getType());
		 */
		/*******************************************/

		boolean response = contactMonitoring(rule);
		qosreservationResponse = new QoSReservationResponse(response, new QoSReservationCommand(message.getService(),
				message.getProvider(), message.getConsumer(), message.getCommands(), message.getRequestedQoS()));

		return qosreservationResponse;
	}

	/**
	 * 
	 * @param provider_network_device
	 * @return
	 */
	private Network_DeviceDTO getNetworkDeviceFromSystem(ArrowheadSystem system) {
		Network_DeviceDTO provider_network_device = scsfactory.getNetworkDeviceFromSystem(system);
		return provider_network_device;
	}

	/**
	 * 
	 * @param provider_network_device
	 * @return
	 */
	public List<QoS_Resource_ReservationDTO> getNetworkDeviceQoSReservations(
			Network_DeviceDTO provider_network_device) {
		if (provider_network_device == null)
			return null;
		return qosfactory.getNetworkDeviceQoSReservations(provider_network_device);
	}

	/**
	 * 
	 * @param system
	 * @param v
	 * @param qosVerificationResponse
	 */
	private void updateQoSVerificationResponse(ArrowheadSystem system, QoSVerifierResponse v,
			QoSVerificationResponse qosVerificationResponse) {
		boolean isPossible = v.getResponse();
		qosVerificationResponse.addResponse(system, v.getResponse());
		if (!isPossible) {
			qosVerificationResponse.addRejectMotivation(system, v.getRejectMotivation());
		}
	}

	/**
	 * 
	 * @param rule
	 * @return
	 */
	private boolean contactMonitoring(QoSMonitorAddRule rule) {
		String strtarget = sysConfig.getMonitorURI() + "/qosrule";
		WebTarget target = client.target(strtarget);
		Response response = target.request().header("Content-type", "application/json").put(Entity.json(rule));
		if (response.getStatus() > 199 && response.getStatus() < 300)
			return true;
		return false;
	}

}
