package eu.arrowhead.core.qos.database;

import eu.arrowhead.core.qos.database.model.ArrowheadSystem;
import eu.arrowhead.core.qos.database.model.Network_Device;
import eu.arrowhead.core.qos.database.model.Node;

public interface ISCSRepository {
	
	public Node getNodeFromSystem(ArrowheadSystem provider);
	
	public Network_Device getNetworkDeviceFromSystem(ArrowheadSystem provider);
	
	public <T> T save(T object);
	
}
