package eu.arrowhead.core.qos.interpreters.stream;

import java.util.HashMap;
import java.util.Map;

import eu.arrowhead.core.qos.drivers.CommunicationProtocols;
import eu.arrowhead.core.qos.interpreter.StreamInterpreter;

public class StreamFactory {
	
	private static StreamFactory instance;
	private static Map<String, StreamInterpreter> list;
	
	private StreamFactory(){
		bootstrap();
	}
	
	public static StreamFactory getInstance(){
		if(instance == null){
			instance = new StreamFactory();
		}
		return instance;
	}
	
	private static void bootstrap(){
		if(list == null) list = new HashMap<>();
		
		list.put(CommunicationProtocols.FTTSE, new FTTSE());
	}

}
