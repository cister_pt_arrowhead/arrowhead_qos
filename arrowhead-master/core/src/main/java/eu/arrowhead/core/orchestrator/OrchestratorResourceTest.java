package eu.arrowhead.core.orchestrator;

import static org.junit.Assert.assertTrue;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.junit.Test;

import eu.arrowhead.common.model.ArrowheadService;
import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.common.model.messages.OrchestrationForm;
import eu.arrowhead.common.model.messages.OrchestrationResponse;
import eu.arrowhead.common.model.messages.ServiceRequestForm;

public class OrchestratorResourceTest {
	
/////////////////////////////////////////////
//// CONFIG /////////////////////////////////
/////////////////////////////////////////////
private String target_IP = "localhost";
private String serviceGroup = "TemperatureProbaTest";
private String serviceDefinition = "IndoorTemperatureProbaTest";
/////////////////////////////////////////////

/**
* Consumer's ArrowheadSystem.
*/
private ArrowheadSystem arrowheadSystem = new ArrowheadSystem("BUTE", "TemperatureProvider", "152.66.183.152", "8080",
"tempcert");

/**
* Stores the OrchestrationForm obtained from the Orchestrator service.
*/
private static OrchestrationForm providerForm = null;


	@Test
	public void test() {
		ServiceRequestForm serviceRequestForm = new ServiceRequestForm();
		Map<String, Boolean> orchestrationFlags = new HashMap<>();
		Response response = null;

		// Preparing ServiceRequestForm
		serviceRequestForm.setRequestedService(getTemperatureService());
		serviceRequestForm.setRequestedQoS(new HashMap<String,String>());
		serviceRequestForm.setRequesterSystem(this.arrowheadSystem);

		// Preparing Orchestration Flags for the ServiceRequestForm
		orchestrationFlags.put("matchmaking", false);
		orchestrationFlags.put("externalServiceRequest", false);
		orchestrationFlags.put("triggerInterCloud", false);
		orchestrationFlags.put("metadataSearch", false);
		orchestrationFlags.put("pingProvider", false);

		serviceRequestForm.setOrchestrationFlags(orchestrationFlags);

		// Invoke the orchestration process and store the reponse
		response = getOrchestrationResponse(serviceRequestForm);
		
		assertTrue(response.getStatus() == 200);	

	}
	
	/**
	 * This function handles the necessary communication through REST to get the
	 * orchestration response from the Orchestrator service.
	 * 
	 * @return void
	 */
	private Response getOrchestrationResponse(ServiceRequestForm serviceRequestForm) {
		Client client = ClientBuilder.newClient();
		URI uri = UriBuilder.fromPath("http://" + target_IP + ":" + "8080").path("core").path("orchestrator")
				.path("orchestration").build();

		WebTarget target = client.target(uri);
		Response response = target.request().header("Content-type", "application/json")
				.post(Entity.json(serviceRequestForm));

		try {
			for (OrchestrationForm form : response.readEntity(OrchestrationResponse.class).getResponse()) {
				if (form.getService().getServiceDefinition().equals(serviceDefinition)) {
					providerForm = form;
				}
			}
			
		} catch (ProcessingException e) {
			return null;
		}

		return response;
	}
	
	/**
	 * This function provides an ArrowheadService required to create a suitable
	 * ServiceRequestForm.
	 * 
	 * @return ArrowheadService
	 */
	private ArrowheadService getTemperatureService() {
		ArrowheadService temperatureService = new ArrowheadService();
		ArrayList<String> interfaces = new ArrayList<String>();

		temperatureService.setServiceGroup(serviceGroup);
		temperatureService.setServiceDefinition(serviceDefinition);
		temperatureService.setMetaData("Dummy metadata");
		interfaces.add("RESTJSON");
		temperatureService.setInterfaces(interfaces);

		return temperatureService;
	}

}
